#!/usr/bin/env python3
import hashlib
import base64

# https://web.archive.org/web/20190120044932/https://twitter.com/alfiedotwtf
q = "Q: What's the difference between a cryptanalyst and a good cryptanalyst?"
# ANS: 33903fa390c7ab9efa37918d458f5e78e00c38df737e1651ad530fde29503af9

# === Part 1 ===
# Try everything!

# Extract every possible substring of question: "a cryptanalyst", "cryptanalyst", "good cryptanalyst", etc.
def substrings(s):
    for i in range(len(s)):
        for j in range(i+1, len(s)):
            yield s[i:j]
subseq_attempts = set(substrings(q))

# Try these phrases too:
more_atttempts = set(["strength", "crypto", "cryptographer", "twitter", "md5", "sha", "sha256", "persistence"])

phrases = subseq_attempts.union(more_atttempts)

# Debug: Write list of all phrases we will try
with open("phrases.txt", "w") as pfile:
     for p in phrases:
         pfile.write(p + '\n')

# Probably sha256, but doesn't hurt to try others.
algs = [hashlib.md5, hashlib.sha1, hashlib.sha224, hashlib.sha256, hashlib.sha3_256]

# Probably UTF-8, but try others too.
to_utf8 = lambda s : s.encode('utf-8')
to_ascii = lambda s : s.encode('ascii')
to_b64 = lambda s : base64.b64encode(s.encode('utf-8'))
to_b32 = lambda s : base64.b32encode(s.encode('utf-8'))
to_b16 = lambda s : base64.b16encode(s.encode('utf-8'))

encodings = [to_utf8, to_ascii, to_b64, to_b32, to_b16]

digests = set()
for alg in algs:
    for enc in encodings:
        digests = digests.union([alg(enc(perm)).hexdigest() for perm in phrases])

# Write out all hashes
with open("hash.txt", "w") as hashfile:
     for d in digests:
         hashfile.write(d + '\n')

# If it was a single phrase, then hash will be in hash.txt
# e.g. grep "33903fa390c7ab9" hash.txt (check directly for hash)



# === Part 2 ===
# Find the *difference* (get it? =P) between everything!
# Assume ANS == hash(a) - hash(b)
# We can solve this efficiently with a little math rather than trying every combination of two hashes.

# My ans: sha256("a cryptanalyst") - sha256("a good cryptanalyst") == 0x5374aaa...
ANS = 0x5374aaa7844398d5d149c22c08d7b2d0e91f5dc1cd54bb9710b4040406ad5ad0

# Alfie's ans... nothing found so far :(
# ANS = 0x33903fa390c7ab9efa37918d458f5e78e00c38df737e1651ad530fde29503af9

# utility function
def rawhex(s):
    return "%x" % s # convert to string without leading '0x'

# if     ANS == phrase1 - phrase2
# => phrase1 == phrase2 + ANS
def undo_arithdiff(s1):
    a = int(s1, 16) # read hex
    b = ANS
    return rawhex(a + b)

# if     ANS == phrase1 ^ phrase2
# => phrase1 == phrase2 ^ ANS
def undo_xordiff(s1):
    a = int(s1, 16) # read hex
    b = ANS
    return rawhex(a ^ b)

undo_diffs = [undo_arithdiff, undo_xordiff]

# Write differences
with open("undo_diffs.txt", "w") as difffile:
    for f in undo_diffs:
        for s1 in digests:
            result = f(s1)
            difffile.write(result + '\n')

# If it was a difference of two hashes, then hash.txt and undo_diffs.txt will have phrase1 in common.
# comm -12 <(sort hash.txt) <(sort undo_diffs.txt)
