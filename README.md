
# Crack the difference

```
Q: What's the difference between a cryptanalyst and a good cryptanalyst?
A: 33903fa390c7ab9efa37918d458f5e78e00c38df737e1651ad530fde29503af9
```
-- [alfie.wtf](https://alfie.wtf) [[Archive]](https://web.archive.org/web/20190120044932/https://twitter.com/alfiedotwtf)

# Attempt 1:

I'm guessing that the punchline relates to words in the original question. E.g. maybe the answer is the hash of "a good cryptanalyst". After trying random words with no luck, I wrote a function `substrings(question)` to extract all possible substrings of the original question in search of one that hashes to the answer.

# Attempt 2:

The question asked to find the *difference* (get it? 😛) between "a cryptanalyst" and "a good cryptanalyst". So maybe the punchline is of the form `ANS == hash(a) - hash(b)`.

We could test all combinations of strings `a` and `b`; however, that would square the number of hashes to try, which is s.l..o...w. Fortunately, while hashes are hard to reverse, the subtraction operator is easily undone: `hash(a) == hash(b) + ANS`. The idea is to output a list of `hash(b) + ANS`, and find a row in common with the table of `hash(a)` from Attempt 1.

# Status:

Despite trying hashes of all possible substrings of the question, as well as differences of substring hashes, I'm yet to find the official punchline. 😞

Here's my own unofficial punchline:
```
sha256("a cryptanalyst") - sha256("a good cryptanalyst") == 5374aaa7844398d5d149c22c08d7b2d0e91f5dc1cd54bb9710b4040406ad5ad0
```

Perhaps I'm close and just need to try different encodings and hash schemes. Or perhaps I'm completely off. Even one character wrong will result in a completely different hash result (that's the whole point), making it impossible to know how far (or close) I really am.
